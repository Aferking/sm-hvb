#include "stdint.h"
#include "error.h"
#include "CANmapping.h"
#include "BSP_CAN.h"
#include "error.h"

#define WARNING D0_ERROR_WARNING
#define CRITICAL D0_ERROR_CRITICAL

const typedef struct
{
    error_t error;
    uint16_t severity;
}errors;

errors errorMapping[] = 
{
    {air1Welded, WARNING},
    {air2Welded, CRITICAL},
    {preFailure, CRITICAL},
    {bmsFault, CRITICAL},
    {imdFault, CRITICAL},
    {bspdFault, CRITICAL},

    {0,0} // MUST BE LAST!
};
void error(error_t error)
{
    uint8_t k = 0;
    while(errorMapping[k].severity != 0)
    {
        if(error == errorMapping[k].error)
        {
            BSP_CAN_TxByte
            (
            errorMapping[k].severity, 
            errorMapping[k].error
            );
        }
    }
}


