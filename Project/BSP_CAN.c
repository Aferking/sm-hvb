#include "stm32f4xx_can.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_adc.h"
#include "stm32f4xx_rcc.h"
#include "BSP_CAN.h"
#include "ioMapping.h"
#include "BSP_LED.h"
#include "globals.h"
#include "CANmapping.h"
/* Constants */
#define RECIEVED_STARTUP_MESSAGE(ID) (ID > 600 && ID < 801)
#define RECIEVED_ERROR_MESSAGE(ID) (ID > 60 && ID < 601)
#define RECIEVED_SENSOR_MESSAGE(ID) (ID > 800 && ID < 1101)
#define RECIEVED_PING_MESSAGE(ID) (ID > 1100 && ID < 1151)
#define RECIEVED_MESSAGE(ID) (ID > 1150 && ID < 2048)

static CanTxMsg txmsg;
static CanRxMsg rxmsg;

void BSP_CAN_init(void) // CAN Setup
{
    GPIO_InitTypeDef        GPIO_InitStructure;
    CAN_InitTypeDef         CAN_InitStructure;
    NVIC_InitTypeDef        NVIC_InitStructure;
    CAN_FilterInitTypeDef   CAN_FilterInitStructure;    
	
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
		
	GPIO_InitStructure.GPIO_Pin = PIN_CAN_RX | PIN_CAN_TX; 
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_100MHz;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_UP;
	GPIO_Init(GPIO_CAN, &GPIO_InitStructure);
	
	//Alternate Function for the GPIOB pins 8 & 9. 
	GPIO_PinAFConfig(GPIO_CAN, PINSOURCE_CAN_TX, GPIO_AF_CAN1);
	GPIO_PinAFConfig(GPIO_CAN, PINSOURCE_CAN_RX, GPIO_AF_CAN1);
	
	//CAN1 enable setup
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_CAN1, ENABLE);

	/* CAN1 register init */
	CAN_DeInit(CAN1);

	/* CAN1 cell init */
	CAN_InitStructure.CAN_TTCM = DISABLE;
	CAN_InitStructure.CAN_ABOM = DISABLE;
	CAN_InitStructure.CAN_AWUM = DISABLE;
	CAN_InitStructure.CAN_NART = DISABLE;
	CAN_InitStructure.CAN_RFLM = DISABLE;
	CAN_InitStructure.CAN_TXFP = DISABLE;
	CAN_InitStructure.CAN_Mode = CAN_Mode_Normal;
	CAN_InitStructure.CAN_SJW = CAN_SJW_3tq;                    //1tq;

	/* CAN1 Baudrate = 500 kBps (CAN1 clocked at 40 MHz)*/      //30MHz 1 MBps
	CAN_InitStructure.CAN_BS1 = CAN_BS1_4tq;                    //6tq;
	CAN_InitStructure.CAN_BS2 = CAN_BS2_2tq;                    //8tq;
	CAN_InitStructure.CAN_Prescaler = (42000000 / 7) / 500000;  //2;
	CAN_Init(CAN1, &CAN_InitStructure);

	/* CAN1 filter init */
	CAN_FilterInitStructure.CAN_FilterNumber = 0;
	CAN_FilterInitStructure.CAN_FilterMode = CAN_FilterMode_IdMask;
	CAN_FilterInitStructure.CAN_FilterScale = CAN_FilterScale_32bit;
	CAN_FilterInitStructure.CAN_FilterIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdHigh = 0x0000;
	CAN_FilterInitStructure.CAN_FilterMaskIdLow = 0x0000;
	CAN_FilterInitStructure.CAN_FilterFIFOAssignment = 0;
	CAN_FilterInitStructure.CAN_FilterActivation = ENABLE;
	CAN_FilterInit(&CAN_FilterInitStructure);

	/* Enable FIFO 0 message pending Interrupt */
	CAN_ITConfig(CAN1, CAN_IT_FMP0, ENABLE);

	NVIC_InitStructure.NVIC_IRQChannel = CAN1_RX0_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0;
    NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
    NVIC_Init(&NVIC_InitStructure);	
}
/**
* @brief Transmit a 16 bit long data over CAN-bus.
* @param ID :   The ID used for the message. Can be a uint32_t
                number,
* @param D0 :   uint8_t used if the message contains a identifier
                in the data 0 field.
* @param halfword   : The data to be transmitted.
*/
void BSP_CAN_TxHalfwordD0(uint32_t ID, uint8_t D0 ,uint16_t halfword)
{
    uint8_t data[3];
    const uint8_t length = 3;
    data[0] = D0;
    data[1] = MASK_LOWER_BYTE(halfword);
    data[2] = MASK_UPPER_BYTE(halfword);
    BSP_CAN_Tx(ID, length, data); 
}

void BSP_CAN_TxHalfword(uint32_t ID,uint16_t halfword)
{
    uint8_t data[2];
    uint8_t length = 2;
    data[0] = MASK_LOWER_BYTE(halfword);
    data[1] = MASK_UPPER_BYTE(halfword);
   
    BSP_CAN_Tx(ID, length, data); 
}

void BSP_CAN_TxByte(uint32_t ID, uint8_t byte)
{
    uint8_t data[1];
    uint8_t length = 1;
    data[0] = byte;
    BSP_CAN_Tx(ID, length, data); 
}


void BSP_CAN_TxID(uint32_t ID)
{
    BSP_CAN_Tx(ID, 0, 0);
}

uint8_t BSP_CAN_Tx(uint32_t ID, uint8_t length, uint8_t data[8]) 
{
    uint8_t i = 0;
    uint16_t counter = 0;
	txmsg.StdId = ID;
	txmsg.IDE = CAN_Id_Standard;
	txmsg.RTR = CAN_RTR_Data;
	txmsg.DLC = length;
	
	for(i=0; i<length;i++){
		txmsg.Data[i] = data[i];
	}
	while((CAN_Transmit(CAN1, &txmsg) == 0) && (counter <= 200))
    {
        counter++;
    }
    if(counter == 200)
    {
        LED_ON(LED2);
        return 0;
    }else
    {
        return 1;
    }
}


void CAN1_RX0_IRQHandler(void)
{
    if(CAN1->RF0R & CAN_RF0R_FMP0)
    {
        CAN_Receive(CAN1, CAN_FIFO0, &rxmsg);
        if(rxmsg.StdId == ID_RX_CURRENT)
        {
            reqCurrent = 1;
        }
        else if(rxmsg.StdId == ID_RX_K1_STATE)
        {
            reqK1 = 1;
        }
        else if(rxmsg.StdId == ID_RX_K2_STATE)
        {
            reqK2 = 1;
        }
        else if(rxmsg.StdId == ID_RX_PRECHARGE_STATE)
        {
            reqPrecharge = 1;
        }
        
    }

}	
       
