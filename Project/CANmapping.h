/*
 ID CAN bus list. 
 See the ID-CAN_BUS document on the drive / the documentation map
 for more information.
 -----------------------------------------------------------------
 Constants starting with ID is the standard ID used on the CAN-BUS.
 Constatns starting with D0 is the data field 0 of the package, used
 for specifing commands and such.
 Constants starting with RX is recieve IDs.
 */
 
#include "stdint.h"

#include "config.h"
#ifndef CANMAPPING_H
#define CANMAPPING_H

/* Warnings, transmit */
#define ID_ERROR
#define D0_ERROR_WARNING
#define D0_ERROR_CRITICAL

/* Request IDs, define*/
#define ID_RX_CURRENT 801
#define ID_RX_K1_STATE 1301
#define ID_RX_K2_STATE 1302
#define ID_RX_PRECHARGE_STATE 1303 

#define ID_TX_CURRENT 841
#define ID_TX_K1 1351
#define ID_TX_K2 1352
#define ID_TX_PRECHARGE 1353

typedef struct
{
    uint16_t flag;
    uint8_t ID;
}RX_Struct;

typedef struct
{
    RX_Struct Rx;
    uint16_t TxID;
}can_mapping_t;

extern can_mapping_t CAN[];

#endif /*END CAN_MSGMAPPING_H*/
