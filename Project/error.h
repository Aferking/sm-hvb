#ifndef ERROR_H
#define ERROR_H

typedef enum
    {
        air1Welded          = 0, /* Contactor 1 welded */
        air2Welded          = 1, /* Contactor 2 welded */
        preFailure          = 2, /* pre charge closed after precharging! */
        bmsFault            = 4, /* BMS fault, system restart needed! */
        imdFault            = 5, /* IMD fault, system restart needed! */
        bspdFault           = 6  /* BSPD fault, system restart needed! */
    } error_t; 
#endif
