#include "BSP_systick.h"
#include "BSP_LED.h"
#include "BSP_ADC_SIMPLE.h"
#include "BSP_CAN.h"
#include "BSP_GPIO.h"
#include "CANmapping.h"

/* Function Prototypes */
static void initSystem(void);
static void current_process(void);
static void contactor_process(uint8_t contactor);
static void precharge_process(void);

#define TIMEOUT 2000
#define OFFSET 163.8f
/* FLAGS */
volatile uint8_t reqCurrent = 0;
volatile uint8_t reqK1 = 0;
volatile uint8_t reqK2 = 0;
volatile uint8_t reqPrecharge = 0;

float valueTest = 0;
float valueTestTest = 0;
/* Main code */
int main(void)
{
    
    initSystem();
    while(1)
    {
        if(reqCurrent == 1)
        {
            reqCurrent = 0;
            current_process();
        }
        else if(reqK1 == 1)
        {
            reqK1 = 0;
            contactor_process(1);
        }
        else if(reqK2 == 1)
        {
            reqK2 = 0;
            contactor_process(2);
        }
        else if(reqPrecharge == 1)
        {
            reqPrecharge = 0;
            precharge_process();
        }
    
        if(clk10ms == CLK_COMPLETE)
        {
        current_process(); 
        clk10ms = CLK_RESET;
        }
    }
} 

static void initSystem(void)
{
    BSP_GPIOinit();
	BSP_systick_init();
	BSP_ADC_SIMPLE_init();
	BSP_CAN_init();
}


static void current_process(void)
{
    uint8_t data[8] = {0};
    uint16_t value = 0;
    float newValue = 0;
    float n = 1;
    float curAvg = 0;  
    
    while(n<=1000)
    {
        newValue = ((float)BSP_ADC_SIMPLE_readChannel(ADC_Channel_4));
        curAvg = curAvg + (newValue - curAvg)/n;
        n++;
    }
    
    value  = (uint16_t) (curAvg + 3.28419f );      
    data[0] = MASK_LOWER_BYTE(value);
    data[1] = MASK_UPPER_BYTE(value);
    BSP_CAN_Tx(ID_TX_CURRENT,8, data);
}

#define OPEN 1
#define CLOSED 0

static void contactor_process(uint8_t contactor)
{
    uint16_t k = 500;
    uint8_t contactor_state = OPEN;
    GPIO_TypeDef* GPIO_OUT_AIR;
    GPIO_TypeDef* GPIO_IN_AIR;
    uint16_t PIN_OUT_AIR;
    uint16_t PIN_IN_AIR;
    uint16_t ID;
    
    if(contactor==1)
    {
        GPIO_OUT_AIR = GPIO_OUT_AIR1;
        PIN_IN_AIR = PIN_IN_AIR1;
        ID = ID_TX_K1;
    }
    else if(contactor == 2)
    {
        GPIO_OUT_AIR = GPIO_OUT_AIR2;
        PIN_IN_AIR = PIN_IN_AIR2;
        ID = ID_TX_K2;
    }
    
    GPIO_WriteBit(GPIO_OUT_AIR, PIN_OUT_AIR, Bit_SET);
    while((k < 1000) && ( k > 0 ) )
    {
        contactor_state = GPIO_ReadInputDataBit(GPIO_IN_AIR, PIN_IN_AIR);
        if(contactor_state == CLOSED)
        {
            k--;
        }
        else if(contactor_state == OPEN)
        {
            k++;
        }
    }
    GPIO_WriteBit(GPIO_OUT_AIR, PIN_OUT_AIR, Bit_RESET);
    BSP_CAN_TxByte(ID,contactor_state);
}

static void precharge_process(void)
{
    uint8_t state = 0;
    state = GPIO_ReadInputDataBit(GPIO_IN_PRECHARGE, PIN_IN_PRECHARGE);
    BSP_CAN_TxByte(ID_RX_PRECHARGE_STATE, state);
}
