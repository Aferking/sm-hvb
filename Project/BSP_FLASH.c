/*
FLASH_START_ADRESS: Starts at sector 11, hopefully the code won't
reach sector 11. In other words bad practice; I think a linker script
should have been written and one of the smaller sections should be used
for variable storage.
Also the EEPROM API from STM should maybe be implemented instead.
If time permits this will be further looked at.
*/
/* Includes */
#include "stm32f4xx.h"
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include "BSP_FLASH.h"

#define FLASH_ADRESS_SENSOR_MIN(i) FLASH_START_ADRESS + (1 + 2*i)*16
#define FLASH_ADRESS_SENSOR_MAX(i) FLASH_START_ADRESS + (2 + 2*i)*16

/* Global variables */
/**
 @brief Deletes all data in sector 11 and writes in total 16 values 
 (LOW and HIGH for wach sensor input) in the start of sector 11.
 
 @param channel  : The channel that is to be calibrated. Can be a uint8_t from 1-8.
 @param data_high: The maximum value of the sensor to be calibrated.

 @param data_low : The minimum value of the sensor to be calibrated.
*/

void BSP_FLASH_storeCalibration(uint16_t data_high[8], uint16_t data_low[8])
{
	uint8_t i;
	FLASH_Unlock();
	FLASH_ClearFlag( FLASH_FLAG_EOP |  FLASH_FLAG_WRPERR | FLASH_FLAG_PGAERR | FLASH_FLAG_PGPERR | FLASH_FLAG_PGSERR);
	FLASH_EraseSector(FLASH_Sector_11, VoltageRange_3); 
	
	/* Used for determening if the calibration has been saved previously */
	FLASH_ProgramHalfWord(FLASH_START_ADRESS, FLASH_PREAMPLE); 
	
	/* Save the data*/
	for(i=0; i<=7; i++)
	{
		FLASH_ProgramHalfWord(FLASH_ADRESS_SENSOR_MIN(i), data_low[i]);
		FLASH_ProgramHalfWord(FLASH_ADRESS_SENSOR_MAX(i), data_high[i]);
	}
	
	/*The flash must be locked again when the writing is finished */
	FLASH_Lock();
}


/**
 * @brief read the calibrated values.
*/
void BSP_FLASH_getCalibratedValues(uint16_t *data_high, uint16_t *data_low)
{
    if(FLASH_SENSOR_DATA_EXISTS)
	{
        int i = 0;
		for(i=0; i<=7; i++)
        {
            data_high[i]  = *(uint16_t*)(FLASH_ADRESS_SENSOR_MAX(i));
            data_low[i]  = *(uint16_t*)(FLASH_ADRESS_SENSOR_MIN(i));		
        }
	}
	else // no data exists
	{
		int k = 0;
		for(k = 0; k <= 7; k++)
		 {
			data_low[k] = 0x0000;
			data_high[k] = 0x0000;	
		}
	}
}
