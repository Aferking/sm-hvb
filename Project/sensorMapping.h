#include "config.h"
#include "stm32f4xx_gpio.h"
#include "stm32f4xx_adc.h"

#ifndef SENSORMAPPING_H
#define SENSORMAPPING_H

#define THROTTLE_INVERTED 0
#define THROTTLE_NON_INVERTED 1

#endif /* END SENSORMAPPING_H*/
