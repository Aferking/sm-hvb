#include "BSP_systick.h"

volatile uint16_t clk1000ms=CLK_RESET ; 
volatile uint16_t clk100ms=CLK_RESET ; 
volatile uint16_t clk10ms=CLK_RESET ; 


void BSP_systick_init(void)
{
	SysTick_Config(SystemCoreClock / 1000);
	//NVIC_SetPriority(SysTick_IRQn, 0);
}

void SysTick_Handler(void)
{
	if (clk1000ms != CLK_COMPLETE){
		clk1000ms++;
		if (clk1000ms >= 1001) clk1000ms = CLK_COMPLETE;
	}

	if (clk100ms != CLK_COMPLETE){
		clk100ms++;
		if (clk100ms >= 101) clk100ms = CLK_COMPLETE;
	}
    
	if (clk10ms != CLK_COMPLETE){
		clk10ms++;
		if (clk10ms >= 11) clk10ms = CLK_COMPLETE;
	}      
}
