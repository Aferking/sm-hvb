#ifndef IOMAPPING_H
#define IOMAPPING_H

/* LEDS */
#define LED_GPIO GPIOD 
#define LED1 GPIO_Pin_0 
#define LED2 GPIO_Pin_2 
#define LED3 GPIO_Pin_1 
#define LED4 GPIO_Pin_3 
#define ALL_LEDS LED1|LED2|LED3|LED4

#define GPIO_CAN_EN GPIOC
#define PIN_CAN_EN GPIO_Pin_10

#define GPIO_CAN GPIOB
#define PIN_CAN_RX GPIO_Pin_8
#define PIN_CAN_TX GPIO_Pin_9
#define PINSOURCE_CAN_RX GPIO_PinSource8
#define PINSOURCE_CAN_TX GPIO_PinSource9

#define GPIO_IN_AIR1 GPIOC
#define PIN_IN_AIR1 GPIO_Pin_7

#define GPIO_IN_AIR2 GPIOC
#define PIN_IN_AIR2 GPIO_Pin_8

#define GPIO_OUT_AIR1 GPIOB
#define PIN_OUT_AIR1 GPIO_Pin_12

#define GPIO_OUT_AIR2 GPIOB
#define PIN_OUT_AIR2 GPIO_Pin_15


#define PIN_IN_PRECHARGE GPIO_Pin_14
#define GPIO_IN_PRECHARGE GPIOB

#endif
