#include "stm32f4xx.h"
#include "stm32f4xx_rcc.h"
#include "stm32f4xx_gpio.h"
#include "ioMapping.h"
#include "BSP_LED.h"

/* Function prototypes */
static void GPIO_enableClock(GPIO_TypeDef* GPIOx);
void GPIO_initPin(
    uint16_t pin, 
    GPIO_TypeDef* GPIOx, 
    GPIOMode_TypeDef mode, 
    GPIOPuPd_TypeDef PuPd);

void BSP_GPIOinit(void)
{
    /* Init LEDS outputs*/
    GPIO_initPin(ALL_LEDS, LED_GPIO, GPIO_Mode_OUT,GPIO_PuPd_NOPULL);
    /* Init can enable output*/
    GPIO_initPin(PIN_CAN_EN, GPIO_CAN_EN,GPIO_Mode_OUT, GPIO_PuPd_NOPULL);
    /* Init AIR 1 output*/
    GPIO_initPin(PIN_OUT_AIR1, GPIO_OUT_AIR1, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);
    /* Init  AIR 2 output */
    GPIO_initPin(PIN_OUT_AIR2, GPIO_OUT_AIR2, GPIO_Mode_OUT, GPIO_PuPd_NOPULL);
    /* Init AIR 1 input */
    GPIO_initPin(PIN_IN_AIR1, GPIO_IN_AIR1, GPIO_Mode_IN, GPIO_PuPd_UP);
     /* Init AIR 2 input */
    GPIO_initPin(PIN_IN_AIR2, GPIO_IN_AIR2, GPIO_Mode_IN, GPIO_PuPd_UP);
    /* Init precharge input */
    GPIO_initPin(PIN_IN_PRECHARGE, GPIO_IN_PRECHARGE, GPIO_Mode_IN, GPIO_PuPd_DOWN);
    
    /* PERMANENTLY ENABLE CAN */
    GPIO_WriteBit(GPIO_CAN_EN, PIN_CAN_EN, Bit_SET);
}

void GPIO_initPin
    (
    uint16_t pin, 
    GPIO_TypeDef* GPIOx, 
    GPIOMode_TypeDef mode, 
    GPIOPuPd_TypeDef PuPd
    )
{
    GPIO_InitTypeDef GPIO_initStruct;
    
    GPIO_enableClock(GPIOx);    
    GPIO_initStruct.GPIO_Mode = mode;
    GPIO_initStruct.GPIO_OType = GPIO_OType_PP;
    GPIO_initStruct.GPIO_PuPd = PuPd;
    GPIO_initStruct.GPIO_Speed = GPIO_Speed_2MHz;
    GPIO_initStruct.GPIO_Pin = pin;
    GPIO_Init(GPIOx, &GPIO_initStruct);
}


static void GPIO_enableClock(GPIO_TypeDef* GPIOx)
{
   if(GPIOx == GPIOA)
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
   else if(GPIOx == GPIOB)
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
   else if(GPIOx == GPIOC)
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
   else if(GPIOx == GPIOD)
   RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
}
